#!/usr/bin/env python

import visa
import classVI

def main():
	rm = visa.ResourceManager('C:\\Windows\\System32\\visa32.dll')
	l = rm.list_resources()
	for i in range(len(rm.list_resources())):
		vi_res = rm.list_resources()[i]
		print "resource number : ",i
		print "resource name   : ", vi_res
		print "detail :"
		try:
			vi = classVI.VIbase(rm, vi_res)
			vi.test()
		except:
			print "cannot read...\n"

if __name__=='__main__':
	main()

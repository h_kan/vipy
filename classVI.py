#!/usr/bin/env python

import visa
import time
import numpy as np

from PyDAQmx.DAQmxFunctions import *
from PyDAQmx.DAQmxConstants import *

class DAQ:
	def __init__(self, physicalChannel, limit = None, reset = False):
		# string -> one component list
		if type(physicalChannel) == type(""):
			self.physicalChannel = [physicalChannel]
		# list -> list
		else:
			self.physicalChannel = physicalChannel
		
		# get list length
		self.numberOfChannel = physicalChannel.__len__()

		# setting of limit
		if limit is None:
			self.limit = dict([(name, (-10.0,10.0)) for name in self.physicalChannel])
		elif type(limit) == tuple:
			self.limit = dict([(name, limit) for name in self.physicalChannel])
		else:
			self.limit = dict([(name, limit[i]) for  i,name in enumerate(self.physicalChannel)])		   
		if reset:
			DAQmxResetDevice(physicalChannel[0].split('/')[0] )

		self.configure()


	def configure(self):
		# Create one task handle per Channel
		taskHandles = dict([(name, TaskHandle(0)) for name in self.physicalChannel])
		for name in self.physicalChannel:
			DAQmxCreateTask("", byref(taskHandles[name]))
			DAQmxCreateAIVoltageChan(taskHandles[name], name, "", DAQmx_Val_Diff,
									 self.limit[name][0], self.limit[name][1],
									 DAQmx_Val_Volts,None)
			DAQmxCfgSampClkTiming(taskHandles[name], "", 1000.0, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, 100)
		self.taskHandles = taskHandles

	def readAll(self):
		#return dict([(name,self.read(name)) for name in self.physicalChannel])
		return list((self.read(name)[0]) for name in self.physicalChannel)

	def read(self, name = None):
		if name is None:
			name = self.physicalChannel[0]
		taskHandle = self.taskHandles[name]					   
		DAQmxStartTask(taskHandle)
		data = numpy.zeros((1,), dtype=numpy.float64)
		# data = AI_data_type()
		read = int32()
		DAQmxReadAnalogF64(taskHandle,1,10.0,DAQmx_Val_GroupByChannel,data,1,byref(read),None)
		DAQmxStopTask(taskHandle)
		return data

	def getVOLT(self, num = None):
		if num == None:
			#name = None
			data = self.readAll()
			return data
		elif num < len(self.physicalChannel):
			name = self.physicalChannel[num]
			data = self.read(name)
			return data[0]
		else:
			return 0
		


class VIbase:
	def __init__(self, rm, name):
		self.vi = rm.get_instrument(name)
		self.ch = 1

	def test(self):
		self.vi.write("*IDN?")
		print self.vi.read()

	def reset(self):
		self.vi.write("RST")

	def cmd(self, command):
		self.vi.write(command)
		if command[-1] == '?':
			print self.vi.read()

	def setCHANNEL(self, ch):
		self.ch = ch

class LakeShore(VIbase):
	def getTemp(self, ch=None):
		if ch==None: ch = self.ch
		self.vi.write('KRDG? %d'%ch)
		T = float(self.vi.read())
		return T

class ADCMT(VIbase):
	def outON(self):
		self.vi.write("OPR")
	
	def outOFF(self):
		self.vi.write("SBY")

	def setVOLT(self, volt):
		self.vi.write("SOV%f"%volt)

	def setCURR(self, current):
		self. vi.write("SOI%f"%current)

	def readVOLT(self):
		self.vi.write('*TRG')
		time.sleep(0.1)
		volt = self.vi.read()
		#print volt
		volt = float(volt[3:-2])
		return volt

	def readCURR(self, rep=1):
		currArr = []
		for i in range(rep):
			self.vi.write('*TRG')
			time.sleep(0.1)
			curr = self.vi.read()
			print "----------"
			print curr
			print "----------"
			currArr.append(float(curr[3:-2]))
		return np.average(np.array(currArr))

	def set4W(self):
		self.cmd("RS1")

	def set2W(self):
		self.cmd("RS0")

	### readVOLT at constantCURR
	def setDCV(self):
		self.cmd("F1")
		self.cmd("IF")

	### readCURR at constantVOLT
	def setDCI(self):
		self.cmd("VF")
		self.cmd("F2")

class PMP(VIbase):
	def setCHANNEL(self, ch):
		self.vi.write("INST:NSEL 2")

	def outON(self):
		self.vi.write("OUTP 1")
	
	def outOFF(self):
		self.vi.write("OUTP 0")

	def setVOLT(self, volt):
		self.vi.write("VOLT %f"%volt)

class Keithly(VIbase):
	def getVOLT(self, ch=None):
		if ch==None: ch = self.ch
		self.vi.write('MEASure? (@10%d)'%ch)
		T_str = self.vi.read()
		T = float(T_str[:15])
		return T
		





#!/usr/bin/env python

import visa
import time
import numpy as np

from PyDAQmx import *


class DAQnew:
	def __init__(self, name, sampRate, dataNum, timeOut=10., limit_abs=10.):
		ind = name.find(':')
		if ind == -1:
			self.channelNum = 1
		else:
			self.channelNum = int(name[ind+1]) - int(name[ind-1]) + 1

		self.sampRate = sampRate
		self.dataNum = dataNum
		self.timeOut = timeOut

		self.analog_input = Task()
		self.read = int32()
		self.data = np.zeros([self.dataNum * self.channelNum], dtype=np.float64)

		self.analog_input.CreateAIVoltageChan(name, "", DAQmx_Val_Cfg_Default, -limit_abs, limit_abs, DAQmx_Val_Volts, None)
		self.analog_input.CfgSampClkTiming("", self.sampRate, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, self.dataNum)

	def getVOLT(self):
		self.analog_input.StartTask()
		self.analog_input.ReadAnalogF64(self.dataNum, self.timeOut, DAQmx_Val_GroupByChannel, self.data, len(self.data), byref(self.read), None)
		return self.data.reshape(self.channelNum, self.dataNum).T

	def stop(self):
		self.analog_input.StopTask()
		self.analog_input.ClearTask()





from PyDAQmx import Task

class CallbackTask(Task):
	def __init__(self, name, sampRate, dataNum, timeOut=10., limit_abs=10.):
		Task.__init__(self)
		
		ind = name.find(':')
		if ind == -1:
			self.channelNum = 1
		else:
			self.channelNum = int(name[ind+1]) - int(name[ind-1]) + 1

		self.sampRate = sampRate
		self.dataNum = dataNum
		self.timeOut = timeOut

		self.data = np.zeros([self.dataNum * self.channelNum], dtype=np.float64)
		self.all_data = []

		#self.analog_input.CreateAIVoltageChan(name, "", DAQmx_Val_Cfg_Default, -limit_abs, limit_abs, DAQmx_Val_Volts, None)
		#self.CreateAIVoltageChan(name, "", DAQmx_Val_RSE, -limit_abs, limit_abs, DAQmx_Val_Volts, None)
		self.CreateAIVoltageChan(name, "", DAQmx_Val_Cfg_Default, -limit_abs, limit_abs, DAQmx_Val_Volts, None)
		self.CfgSampClkTiming("", self.sampRate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, self.dataNum)
		self.AutoRegisterEveryNSamplesEvent(DAQmx_Val_Acquired_Into_Buffer,self.dataNum,0)
		self.AutoRegisterDoneEvent(0)

	def EveryNCallback(self):
		self.read = int32()
		self.ReadAnalogF64(self.dataNum, self.timeOut, DAQmx_Val_GroupByChannel, self.data, len(self.data), byref(self.read), None)
		self.all_data.append(self.data.tolist())
		#print self.data[0]
		return 0 # The function should return an integer

	def DoneCallback(self, status):
		print "Status",status.value
		return 0 # The function should return an integer




class DAQ_Callback:
	def __init__(self, name, sampRate, dataNum, timeOut=10., limit_abs=10.):
		self.task = CallbackTask(name, sampRate, dataNum, timeOut=10., limit_abs=10.)
		self.sampling_rate = sampRate
		self. data_num = dataNum

	def getVOLT(self):
		self.task.StartTask()
		raw_input('Acquiring samples continuously. Press Enter to interrupt\n')
		self.task.StopTask()
		#return np.array(self.task.all_data).reshape(self.task.channelNum,-1).T
		return np.array(self.task.all_data)

	def stop(self):
		self.task.ClearTask()



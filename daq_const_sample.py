#!/usr/bin/env python

import matplotlib.pyplot as plt
import time
import numpy as np
import visa
import sys

import classVI_daq

import threading

def get(daq):
	V = daq.getVOLT()

def save(filename, daq):
	### copy array
	V = np.array(daq.task.all_data) * 1
	### delete original array
	del daq.task.all_data[:]

	channelNum = daq.task.channelNum
	sampling_rate = daq.sampling_rate
	data_num = daq.data_num

	print V.size/data_num/channelNum
	newV = np.zeros([channelNum, V.size/channelNum])
	tmpV = V.reshape(V.size/data_num, -1)
	for i in range(channelNum):
		for j in range(V.size/channelNum/data_num):
			newV[i][j*data_num:(j+1)*data_num] = tmpV[i+j*channelNum]
	newV = newV.T
	print newV.shape
	f = file(filename,'a')
	np.savetxt(f, newV)



def main(rpm="---"):
	filename = time.strftime('./data/rot%m%d%H%M') + '_%srmp_data.txt'%rpm

	dev = "Dev1/ai3"
	sampling_rate = 5000.
	data_num = int(sampling_rate / 10)

	daq = classVI_daq.DAQ_Callback(dev, sampling_rate, data_num)

	first_time = time.time()

	th = threading.Thread(target=get, args=(daq,))
	th.setDaemon(True)
	th.start()

	time.sleep(0.1)
	thshold = -0.5
	xx = []
	ff = []
	try:
		counter = 0
		start_time = time.time()
		while True:
			counter += 1
			time.sleep(0.001)
			print len(daq.task.all_data)
			leng = len(daq.task.all_data)

			if leng > 100:
				save(filename, daq)


			### plot wave
			plt.subplot(2,1,1)
			n = 5
			x = np.arange(n*data_num)
			y = np.zeros(n*data_num)
			if len(daq.task.all_data) > n:
				y = np.array(daq.task.all_data[-n:]).reshape(n*data_num)
				plt.plot(x,y)

				### plot frequency
				plt.subplot(2,1,2)
				current_time = time.time() - start_time
				ind1 = np.where(y[1:]<thshold)
				ind2 = np.where(y[:-1]>thshold)

				ind = np.intersect1d(ind1,ind2)
				print "ind",ind
				time_d = (ind[1:] - ind[:-1]) / sampling_rate
				ff.extend(1./time_d/60.)
				xx.extend(ind[:-1]/sampling_rate + current_time)
				if len(xx) > 300:
					del xx[:-300]
					del ff[:-300]
				plt.plot(xx,ff,'.')
			else:
				plt.subplot(2,1,2)
				plt.plot(xx,ff,'.')

			
			#plt.ylim([-1.2, 0.1])
			plt.pause(0.001)
			if counter == 1:
				plt.clf()
				counter = 0


	
	except KeyboardInterrupt:
		print "finish getting"
		#print time.time() - first_time
		save(filename, daq)
		daq.stop()

		exit()

	
if __name__=='__main__':
	args = sys.argv
	if len(args)>1:
		main(args[1])
	else:
		main()


#!/usr/bin/env python

import visa
import time
import numpy as np
import matplotlib.pyplot as plt
import threading

import classVI

def getIV(vi1, name, Vmin=0., Vmax=2., dV=0.1):
	vi1.setVOLT(0)
	vi1.outON()
	V = np.arange(Vmin, Vmax+dV, dV)
	I = np.zeros(len(V))
	for i, volt in enumerate(V):
		vi1.setVOLT(volt)
		time.sleep(0.05)
		I[i] = vi1.readCURR()
		print "i, V, I, R =", i, volt, I[i], volt/I[i]

	vi1.outOFF()
	vi1.setVOLT(0)

	data = np.vstack((V,I)).T
	np.savetxt('%s.csv'%name, data, delimiter=',')
	print "save text complete"

	plt.xlabel('Voltage [V]')
	plt.ylabel('Current [mA]')
	plt.title('I-V curve of %s'%name)
	plt.plot(V,I * 10**3)
	plt.plot(V,I * 10**3, 'o')
	plt.savefig("%s.png"%name)
	plt.show()


def main():
	rm = visa.ResourceManager('C:\\Windows\\System32\\visa32.dll')
	adcmt1 = classVI.ADCMT(rm, 'GPIB1::1::INSTR') #LED

	adcmt1.test()
	adcmt1.set2W()
	adcmt1.setDCI()
	
	getIV(adcmt1,'LED1')


if __name__=='__main__':
	main()


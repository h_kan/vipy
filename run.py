#!/usr/bin/env python

import time
import numpy as np

import classVI
		

def main():
	filename = 'data.csv'
	curr1 = 10*10**-6 ### for temperature
	curr2 = 0.1 ### for coil

	first_time = time.time()

	rm = visa.ResourceManager('C:\\Windows\\System32\\visa32.dll')
	l = rm.list_resources()
	print(l)

	ls = classVI.LakeShore(rm, 'GPIB0::12::INSTR') ###	LakeShore
	ky = classVI.Keithly(rm, 'GPIB0::16::INSTR') ### Keithly
	adc1 = classVI.ADCMT('GPIB3::3::INSTR')
	adc2 = classVI.ADCMT('GPIB1::1::INSTR')
	
	daq = classVI.DAQ(["Dev1/ai0","Dev1/ai1"]) ### USBDAQ

	adc1.setDCV()
	adc2.setDCV()
	adc1.setCURR(curr1)
	adc2.setCURR(curr2)
	adc1.outON()
	adc2.outON()

	while True:
		current_time = time.time() - first_time
		arr = [current_time]
		for i in range(4):
			T = ky.getVOLT(i+1)
			arr.append(T)
		for i in range(4):
			T = ls.getTemp(i+5)
			arr.append(T)
		V = daq.getVOLT() # return 2 component list
		arr = arr + V

		#print arr
		f = file(filename,'a')
		np.savetxt(f, arr, delimiter=',')

	adc1.outOFF()
	adc2.outOFF()

if __name__=='__main__':
		main()

#!/usr/bin/env python

import visa
import classVI

def main():
	rm = visa.ResourceManager('C:\\Windows\\System32\\visa32.dll')
	pmp = classVI.PMP(rm, 'USB0::0x0B3E::0x1011::XC001388::INSTR')
	adcmt1 = classVI.ADCMT(rm, 'GPIB1::1::INSTR') #LED
	adcmt2 = classVI.ADCMT(rm, 'GPIB3::3::INSTR') #SiPD
	
	pmp.setVOLT(0)
	adcmt1.setVOLT(0)
	adcmt2.setVOLT(0)
	pmp.outOFF()
	adcmt1.outOFF()
	adcmt2.outOFF()

if __name__=='__main__':
	main()

